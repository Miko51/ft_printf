/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 20:10:26 by muhaaydi          #+#    #+#             */
/*   Updated: 2022/12/15 16:54:26 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

static size_t	count_digit(long n)
{
	size_t	c;

	c = 0;
	if (n < 0)
	{
		n *= -1;
		c++;
	}
	while (n >= 0)
	{
		n /= 10;
		c++;
		if (n == 0)
			break ;
	}
	return (c);
}

char	*ft_uitoa(unsigned int n)
{
	unsigned int	n2;
	size_t			len;
	char			*toarray;

	n2 = n;
	len = count_digit(n2);
	toarray = malloc(sizeof(char) * (len + 1));
	if (!toarray)
		return (NULL);
	toarray[len--] = '\0';
	while (n2 >= 0)
	{
		toarray[len] = (n2 % 10) + '0';
		n2 /= 10;
		len--;
		if (n2 == 0)
			break ;
	}
	return (toarray);
}
