[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Ft_printf

**In this project, the printf function is being emulated. Topics such as data types, number systems, and the logic of printing data are learned.**

## Usage
Clone the project and make it. After that, an archive file named libftprintf.a will be generated. You can compile your code with this file to use the ft_printf function.

## Score
The project has passed with a score of 100%. Therefore, character, string, pointer, integer, unsigned integer, hexadecimal numbers (can be used as %x and %X), and % conversions are supported. [Project subject](en.subject.pdf)
