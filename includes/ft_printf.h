/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 20:12:34 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/01/09 14:52:08 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define    FT_PRINTF_H
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>
# include <limits.h>

char	*ft_itoa(int n);
char	*ft_uitoa(unsigned int n);
size_t	ft_strlen(char *chr);
size_t	ft_putstr_fd(char *s, int fd, int free);
int		ft_putchar_fd(char c, int fd);
int		ft_printf(const char *str, ...);
#endif
