[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Ft_printf

**Bu projede printf fonksiyonu taklit edilmektedir. Projede veri tipleri, sayı sistemler, verileri print etme mantığı gibi konular öğrenilir.**

## Kullanım

Projeyi klonlayıp make edin. Ardından libftprintf.a adında bir arşiv dosyası oluşacaktır. Kodlarınızı bu dosya ile derleyerek ft_printf fonksiyonunu kullanabilirsiniz.  

## Not
Proje notu %100 ile geçmiştir. Bu yüzden karakter, string, pointer, tam sayı, işaretsiz tam sayı, onaltılık sayılar(%x ve %X şeklinde kullanılabilir) ve %% dönüşümleri yapabilir. [Projenin detaylı yönergesi](tr.subject.pdf)
